// ****************** TRANSLATOR FROM ENGLISH TO ZULU *********************




// Variables :: myMap : storage
// ----------------------------------------------------------------------------------------------------------------------------
    let myMap = new Map();
    let word = prompt("Please enter the English word that you would like to translate to Zulu.:");
// ----------------------------------------------------------------------------------------------------------------------------

// Keys and values that are stored in myMap.
// ----------------------------------------------------------------------------------------------------------------------------
    myMap.set("dog", "inja");
    myMap.set("house", "indlu");
    myMap.set("money", "imali");
    myMap.set("car", "imoto");
    myMap.set("computer", "ikhompuyutha");
    myMap.set("technology", "ubuchwepheshe");
    myMap.set("smartphone", "ifoni ehlakaniphile");
    myMap.set("food", "ukudla");
    myMap.set("ocean", "ulwandle");
    myMap.set("gratitude", "ukubonga");
// ----------------------------------------------------------------------------------------------------------------------------

// The returned values of the given keys.
// ----------------------------------------------------------------------------------------------------------------------------
    for (let value of myMap.values())
        {
          console.log("The Zulu word for" +" "+ `${word}` + " " + "is" + " " + myMap.get(`${word}`));
        }
// ----------------------------------------------------------------------------------------------------------------------------
